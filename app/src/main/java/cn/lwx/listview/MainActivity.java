package cn.lwx.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ListView mListView;
    //定义需要适配的数据
    private String[] names = {"京东商城", "QQ", "QQ斗地主", "新浪微博", "天猫", "UC浏览器", "微信"};
    //定义图片集合
    private int[] icons = {R.drawable.jd, R.drawable.qq, R.drawable.dz,
            R.drawable.xl, R.drawable.tm, R.drawable.uc, R.drawable.wx};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.market);

        //初始化ListView控件
        mListView = (ListView) findViewById(R.id.lv);
        //ListView展示数据需要适配器 创建一个Adapter的实例
        MyBaseAdapter mAdapter = new MyBaseAdapter();
        //设置Adapter
        mListView.setAdapter(mAdapter);
    }

    class MyBaseAdapter extends BaseAdapter {//BaseAdapter需要实现4个方法

        //返回item总数
        @Override
        public int getCount() {
            //返回ListView Item条目的总数
            return names.length;
        }

        //得到Item代表的对象
        @Override
        public Object getItem(int position) {
            //返回ListView Item条目代表的对象
            return names[position];
        }

        //得到Item的id
        @Override
        public long getItemId(int position) {
            //返回ListView Item的id
            return position;
        }

        // ListView优化 得到Item的View视图
        @Override // convertView  历史缓存对象
        public View getView(int position, View convertView, ViewGroup parent) {
            //将list_item布局转换成一个View对象 打气筒 View.inflate();
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getApplicationContext()).
                        inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.mTextView = (TextView) convertView.findViewById
                        (R.id.item_tv);
                holder.imageView = (ImageView) convertView.findViewById(R.id.item_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.mTextView.setText(names[position]);
            holder.imageView.setBackgroundResource(icons[position]);
            return convertView;
        }

        class ViewHolder {
            TextView mTextView;
            ImageView imageView;
        }

//        //得到Item的View视图
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            //将list_item布局转换成一个View对象 打气筒 View.inflate();
//            View view = View.inflate(MainActivity.this, R.layout.list_item, null);
//            //找到条目中的控件
//            ImageView imageView = (ImageView) view.findViewById(R.id.item_image);
//            TextView mTextView = (TextView) view.findViewById(R.id.item_tv);
//            mTextView.setText(names[position]);
//            imageView.setBackgroundResource(icons[position]);
//            return view;
//        }

    }

}